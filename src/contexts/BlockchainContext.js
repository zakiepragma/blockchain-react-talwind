import { createContext, useContext, useState } from "react";
import sha256 from "sha256";

const BlockchainContext = createContext();

export const useBlockchain = () => useContext(BlockchainContext);

export const BlockchainProvider = ({ children }) => {
  const [blockchain, setBlockchain] = useState([
    {
      index: 0,
      transaction: "genesis block",
      hash: "0",
    },
  ]);

  const addBlock = (transaction) => {
    const index = blockchain.length;
    const previousHash = blockchain[index - 1].hash;
    const hash = sha256(previousHash + JSON.stringify(transaction));
    setBlockchain([...blockchain, { index, transaction, hash }]);
  };
  return (
    <BlockchainContext.Provider value={{ blockchain, addBlock }}>
      {children}
    </BlockchainContext.Provider>
  );
};
