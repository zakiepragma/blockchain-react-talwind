import React from "react";
import BlockchainList from "./components/BlockchainList";
import TransactionForm from "./components/TransactionForm";
import { BlockchainProvider } from "./contexts/BlockchainContext";

const App = () => {
  return (
    <BlockchainProvider>
      <div className="flex flex-col items-center justify-center min-h-screen bg-gray-100">
        <h1 className="text-3xl font-bold underline mb-4">Hello blockchain!</h1>
        <TransactionForm />
        <BlockchainList />
      </div>
    </BlockchainProvider>
  );
};

export default App;
