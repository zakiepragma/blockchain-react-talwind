import React, { useState } from "react";
import { useBlockchain } from "../contexts/BlockchainContext";

const TransactionForm = () => {
  const { addBlock } = useBlockchain();
  const [transaction, setTransaction] = useState("");

  const handleSubmit = (event) => {
    event.preventDefault();
    addBlock(transaction);
    setTransaction("");
  };

  return (
    <form onSubmit={handleSubmit} className="mt-4">
      <div className="flex items-center mb-4 gap-4">
        <input
          className="p-2 border rounded-md w-3/4"
          type="text"
          placeholder="Enter transaction"
          value={transaction}
          onChange={(event) => setTransaction(event.target.value)}
        />
        <button
          className="bg-blue-500 text-white py-1 px-2 rounded-md hover:bg-blue-600 focus:outline-none focus:ring-2 focus:ring-blue-500 focus:opacity-50"
          type="submit"
        >
          Add Block
        </button>
      </div>
    </form>
  );
};

export default TransactionForm;
