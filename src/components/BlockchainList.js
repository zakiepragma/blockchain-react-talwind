import React, { useContext } from "react";
import { useBlockchain } from "../contexts/BlockchainContext";

const BlockchainList = () => {
  const { blockchain } = useBlockchain();
  return (
    <div className="flex flex-col items-center mt-8">
      {blockchain.map((block, index) => (
        <div key={index} className="mb-4 flex items-center">
          <div className="bg-gray-800 w-12 h-12 text-center rounded-full flex items-center justify-center text-white">
            {block.index}
          </div>
          <div className="bg-gray-400 flex-1 ml-4 p-4 rounded-lg">
            <div>Transaction : {block.transaction}</div>
            <div>Hash : {block.hash}</div>
          </div>
        </div>
      ))}
    </div>
  );
};

export default BlockchainList;
